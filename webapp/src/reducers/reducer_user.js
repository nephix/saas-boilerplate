import { AUTH_USER, SIGN_OUT_USER, AUTH_ERROR, PROCESSING_USER } from '../actions/userActionTypes';

const defaultState = {
	isAuthenticated: false,
	isLoading: true,
	isSigningIn: false,
	error: null,
	email: null,
	uid: null,
	subscription: {
		plan: null,
		expiry: null
	}
};

const userReducer = (state = defaultState, action) => {
	switch (action.type) {
		case AUTH_USER: {
			const { email, uid } = action.payload;

			return {
				...state,
				isAuthenticated: true,
				isLoading: false,
				isSigningIn: false,
				error: null,
				email,
				uid
			};
		}

		case SIGN_OUT_USER:
			return {
				...state,
				isAuthenticated: false,
				isLoading: false,
				isSigningIn: false,
				error: null
			};

		case PROCESSING_USER:
			return {
				...state,
				isSigningIn: true
			};

		case AUTH_ERROR:
			return {
				...state,
				isSigningIn: false,
				error: action.payload.message
			};
	}

	return state;
};

export default userReducer;
