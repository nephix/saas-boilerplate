import firebase from 'firebase';
import 'firebase/firestore';

const config = {
	apiKey: process.env.PREACT_APP_FIREBASE_API_KEY,
	authDomain: process.env.PREACT_APP_FIREBASEAUTH_DOMAIN,
	databaseURL: process.env.PREACT_APP_FIREBASE_DATABASE_URL,
	projectId: process.env.PREACT_APP_FIREBASE_PROJECT_ID,
	storageBucket: process.env.PREACT_APP_FIREBASE_STORAGE_BUCKET,
	messagingSenderId: process.env.PREACT_APP_FIREBASE_MESSAGE_SENDER_ID
};

let app;
if (!firebase.apps.length) {
	app = firebase.initializeApp(config);
}
const googleProvider = new firebase.auth.GoogleAuthProvider();
const firestore = firebase.firestore();

export { app, googleProvider, firebase, firestore };
