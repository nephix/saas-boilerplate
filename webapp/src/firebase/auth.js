import { app, googleProvider } from './firebase';
import 'firebase/auth';

const auth = app.auth();

// Sign up
export const doCreateUserWithEmailAndPassword = (email, password) => auth.createUserWithEmailAndPassword(email, password);

// Sign in
export const doSignInWithEmailAndPassword = (email, password) => auth.signInWithEmailAndPassword(email, password);

// Sign in with Google
export const doSignInWithGoogle = () => auth.signInWithRedirect(googleProvider);

// Sign out
export const doSignOut = () => auth.signOut();

// Password Reset
export const doPasswordReset = email => auth.sendPasswordResetEmail(email);

// Password Change
export const doPasswordUpdate = password => auth.currentUser.updatePassword(password);

// On auth state change
export const onAuthStateChanged = callback => auth.onAuthStateChanged(callback);

// Return current user
export const getCurrentUser = auth.currentUser;
