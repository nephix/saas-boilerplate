// Route components
import Login from '../routes/login';
import SignUp from '../routes/signUp';
import Dashboard from '../routes/dashboard';
import Profile from '../routes/profile';
import Setup from '../routes/setup';

// Private routes
export const privateRoutes = {
	Dashboard: {
		key: 'Dashboard',
		component: Dashboard,
		path: '/dashboard'
	},
	Profile: {
		component: Profile,
		path: '/profile'
	}
};

// Public routes
export const publicRoutes = {
	Login: {
		component: Login,
		path: '/'
	},
	SignUp: {
		component: SignUp,
		path: '/signup'
	},
	Setup: {
		component: Setup,
		path: '/setup'
	}
};
