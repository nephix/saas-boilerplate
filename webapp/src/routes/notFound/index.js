import { h, Component } from 'preact';

const NotFound = () => <div>404, not found</div>;

export default NotFound;
