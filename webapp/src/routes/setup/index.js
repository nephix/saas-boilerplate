import { h, Component } from 'preact';

export default class Setup extends Component {
	render() {
		return (
			<div>
				<h1>Setup your account</h1>
				<form>
					<input ref={input => (this.companyName = input)} type="text" name="companyName" placeholder="Company Name" required />
					<input ref={input => (this.zip = input)} type="text" name="address2" placeholder="Zip" required />
				</form>
			</div>
		);
	}
}
