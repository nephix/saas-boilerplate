import { h } from 'preact';
import { Link } from 'react-router-dom';

import SignUpForm from '../../components/signUpForm';
import GoogleSignInButton from '../../components/googleSignInButton';
import { publicRoutes } from '../../config/routes';

const SignUp = () => (
	<div>
		<h1>Sign up page</h1>
		<SignUpForm />
		<GoogleSignInButton />
		<p>
			Already have an account? <Link to={publicRoutes.Login.path}>Login</Link>
		</p>
	</div>
);

export default SignUp;
