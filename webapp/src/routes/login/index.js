import { h } from 'preact';
import { Link } from 'react-router-dom';

import LoginForm from '../../components/loginForm';
import { publicRoutes } from '../../config/routes';
import GoogleSignInButton from '../../components/googleSignInButton';

const LoginPage = () => (
	<div>
		<h1>Login</h1>
		<LoginForm />
		<GoogleSignInButton />
		<p>
			Don't have an account? <Link to={publicRoutes.SignUp.path}>Sign up</Link>
		</p>
	</div>
);

export default LoginPage;
