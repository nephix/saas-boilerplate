import { AUTH_USER, SIGN_OUT_USER, AUTH_ERROR, PROCESSING_USER } from './userActionTypes';
import { auth, firestore } from '../firebase';

export function signInUser(credentials) {
	return function(dispatch) {
		dispatch(processingUser());
		auth
			.doSignInWithEmailAndPassword(credentials.email, credentials.password)
			.then(response => {
				dispatch(authUser(response.user));
			})
			.catch(error => {
				dispatch(authError(error));
			});
	};
}

export function signUpUser(credentials) {
	return function(dispatch) {
		dispatch(processingUser());
		auth
			.doCreateUserWithEmailAndPassword(credentials.email, credentials.password)
			.then(response => {
				dispatch(authUser(response.user));
			})
			.catch(error => {
				dispatch(authError(error));
			});
	};
}

export const verifyAuth = () => dispatch =>
	auth.onAuthStateChanged(user => {
		if (user) {
			dispatch(authUser(user));
		}
		else {
			dispatch(signOutUser());
		}
	});

export const signOutUser = () => dispatch =>
	auth.doSignOut().then(() => {
		dispatch({
			type: SIGN_OUT_USER
		});
	});

export const authUser = user => ({
	type: AUTH_USER,
	payload: user
});

export const authError = error => ({
	type: AUTH_ERROR,
	payload: error
});

export const processingUser = () => ({ type: PROCESSING_USER });
