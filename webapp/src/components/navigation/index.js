import { h, Component } from 'preact';
import { NavLink } from 'react-router-dom';
import { connect } from 'preact-redux';
import { bindActionCreators } from 'redux';

import { signOutUser } from '../../actions/user';
import { privateRoutes } from '../../config/routes';

class Navigation extends Component {
	onClickSignOut() {
		this.props.signOutUser();
	}

	constructor(props) {
		super(props);
		this.onClickSignOut = this.onClickSignOut.bind(this);
	}

	render() {
		return (
			<nav>
				<ul>
					<li>
						<NavLink to={privateRoutes.Dashboard.path}>Dashboard</NavLink>
					</li>
					<li>
						<NavLink to={privateRoutes.Dashboard.path}>Boards</NavLink>
					</li>
					<li>
						<NavLink to={privateRoutes.Profile.path}>Profile</NavLink>
					</li>
					<li>
						<a href="#" onClick={this.onClickSignOut}>
							Sign out
						</a>
					</li>
				</ul>
			</nav>
		);
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ signOutUser }, dispatch);
}
export default connect(
	null,
	mapDispatchToProps
)(Navigation);
