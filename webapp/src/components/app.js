import { h, Component } from 'preact';
import { Provider } from 'preact-redux';

import store from '../store';
import Template from './template';

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<Template />
			</Provider>
		);
	}
}

export default App;
