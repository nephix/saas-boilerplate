import { h, Component } from 'preact';

export default class PublicLayout extends Component {
	render() {
		const { route } = this.props;
		const ChildComponent = this.props.component;
		return (
			<div>
				<h1>Public Layout</h1>
				<ChildComponent route={route} />
			</div>
		);
	}
}
