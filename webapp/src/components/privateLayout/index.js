import { h, Component } from 'preact';

import Navigation from '../navigation';

export default class PrivateLayout extends Component {
	render() {
		const Component = this.props.component;
		const { route } = this.props;

		return (
			<div>
				<Navigation />
				<main>
					<h1>Hey</h1>
					<Component route={route} />
				</main>
			</div>
		);
	}
}
