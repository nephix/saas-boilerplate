import { h, Component } from 'preact';
import { connect } from 'preact-redux';
import { bindActionCreators } from 'redux';

import { signInUser } from '../../actions/user';

class LoginForm extends Component {
	onSubmit = event => {
		event.preventDefault();
		const { email, password } = this;
		this.props.signInUser({ email: email.value, password: password.value });
		email.value = '';
		password.value = '';
	}

	render() {
		const { error, isLoading } = this.props.user;

		return (
			<form onSubmit={event => this.onSubmit(event)}>
				<input
					ref={emailInput => (this.email = emailInput)}
					type="email"
					name="email"
					placeholder="Your email"
					required
					disabled={isLoading}
				/>
				<input
					ref={passwordInput => (this.password = passwordInput)}
					type="password"
					name="password"
					placeholder="Your password"
					required
					disabled={isLoading}
				/>
				{error ? <div>{error}</div> : ''}
				<button disabled={isLoading}>Login</button>
			</form>
		);
	}
}

const mapStateToProps = (state, props) => state;

const mapDispatchToProps = dispatch => bindActionCreators({ signInUser }, dispatch);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(LoginForm);
