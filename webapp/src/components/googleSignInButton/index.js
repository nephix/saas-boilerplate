import { h, Component } from 'preact';

import { auth } from '../../firebase';

export default class GoogleSignInButton extends Component {
	onClickSignInWithGoogle = () => auth.doSignInWithGoogle()

	render() {
		return (
			<a href="#" onClick={() => this.onClickSignInWithGoogle()}>
				Or sign up with google
			</a>
		);
	}
}
