import { h, Component } from 'preact';
import { connect } from 'preact-redux';
import { bindActionCreators } from 'redux';

import { signUpUser } from '../../actions/user';

class SignUpForm extends Component {
	onSubmit = event => {
		event.preventDefault();
		const { email, password } = this;
		this.props.signUpUser({ email: email.value, password: password.value });
		email.value = '';
		password.value = '';
	}

	render() {
		const { error, isLoading } = this.props.user;
		return (
			<form onSubmit={e => this.onSubmit(e)}>
				<input
					ref={emailInput => (this.email = emailInput)}
					type="email"
					name="email"
					placeholder="Your email"
					required
					disabled={isLoading}
				/>

				<input
					ref={passwordInput => (this.password = passwordInput)}
					type="password"
					name="password"
					placeholder="Type a password"
					required
					disabled={isLoading}
				/>

				{error ? <div>{error}</div> : ''}

				<button disabled={isLoading}>Sign up</button>
			</form>
		);
	}
}

const mapStateToProps = (state, props) => state;

const mapDispatchToProps = dispatch => bindActionCreators({ signUpUser }, dispatch);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SignUpForm);
