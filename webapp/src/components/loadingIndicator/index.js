import { h } from 'preact';

const LoadingIndicator = () => <div>Loading...</div>;

export default LoadingIndicator;
