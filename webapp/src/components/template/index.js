import { h, Component } from 'preact';
import { connect } from 'preact-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import LoadingIndicator from '../loadingIndicator';
import PublicLayout from '../publicLayout';
import PrivateLayout from '../privateLayout';
import { publicRoutes, privateRoutes } from '../../config/routes';
import { verifyAuth } from '../../actions/user';
import Login from '../../routes/login';
import Setup from '../../routes/setup';
import NotFound from '../../routes/notFound';

class Template extends Component {
	constructor(props) {
		super(props);
		this.renderPublicRoute = this.renderPublicRoute.bind(this);
		this.renderPrivateRoute = this.renderPrivateRoute.bind(this);
	}

	componentDidMount() {
		this.props.verifyAuth();
	}

	renderPublicRoute(route, component, path) {
		const { user } = this.props;
		if (user.isAuthenticated) {
			if (user.isSubscriber) {
				return <Redirect to={privateRoutes.Dashboard.path} />;
			}
			return <PublicLayout component={Setup} route={route} />;
		}

		return <PublicLayout component={component} route={route} />;
	}

	renderPrivateRoute(route, component, path) {
		const { user } = this.props;
		if (user.isAuthenticated) {
			if (user.isSubscriber) {
				return <PrivateLayout component={component} route={route} />;
			}
			return <PublicLayout component={Setup} route={route} />;
		}

		return <PublicLayout component={Login} route={route} />;
	}

	render() {
		const { user } = this.props;

		if (user.isLoading) {
			return <LoadingIndicator />;
		}

		return (
			<BrowserRouter>
				<Switch>
					{Object.keys(publicRoutes).map(key => {
						const route = publicRoutes[key];
						const { component, path } = route;
						return <Route exact path={path} key={key} render={route => this.renderPublicRoute(route, component, path)} />;
					})}

					{Object.keys(privateRoutes).map(key => {
						const route = privateRoutes[key];
						const { component, path } = route;
						return <Route exact path={path} key={key} render={route => this.renderPrivateRoute(route, component, path)} />;
					})}

					<Route component={NotFound} />
				</Switch>
			</BrowserRouter>
		);
	}
}

function mapStateToProps(state, props) {
	return state;
}
function mapDispatchToProps(dispatch) {
	return bindActionCreators({ verifyAuth }, dispatch);
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Template);
